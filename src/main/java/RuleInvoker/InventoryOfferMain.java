package RuleInvoker;

import java.util.List;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.vtrio.model.Product;
import com.vtrio.util.CSVToJavaUtil;

public class InventoryOfferMain {

	public static void main(String[] args) throws Exception {

		KieContainer kc = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kc.newKieSession("ksession-rules");

		List<Product> products = CSVToJavaUtil.getProdutsFromCsv();
		for (Product product : products) {
			ksession.insert(product);
		}

		ksession.startProcess("com.vtrio.process.TestJbpm");

		ksession.fireAllRules();

	}

}
