package com.vtrio.model;

import java.util.ArrayList;

public class RiskyAccounts {
	
	private ArrayList<Account> accounts = new ArrayList();

	public void add(Account acc) {
		accounts.add(acc);
	}

	public void listRiskyAccounts() {
		for (Account acc : accounts)
			System.out.println(acc);
	}
}