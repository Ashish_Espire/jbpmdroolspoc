package com.vtrio.ruleinvoker;

import java.util.Date;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.vtrio.model.Product;

public class ProductRuleMain {

	public static void main(String[] args) throws Exception {	
		
		
	
		KieContainer kc = KieServices.Factory.get().getKieClasspathContainer();
	        KieSession ksession2 = kc.newKieSession("ksession-rules");
		Product product = new Product();
		product.setProductId(1l);
		Date orderDate = new Date();
		orderDate.setDate(25);
		orderDate.setMonth(11);
		product.setOrderDate(orderDate);
		
		Product product2 = new Product();
		product2.setProductId(2l);

		Date orderDate2 = new Date();
		product2.setOrderDate(orderDate2);
		product2.setSale(true);
		
		Product product3 = new Product();
		product3.setProductId(2l);

		Date orderDate3 = new Date();
		product3.setOrderDate(orderDate3);
		product3.setProductType("gold");
		product3.setPrice(15000l);
		
		
		ksession2.insert(product);
		ksession2.insert(product2);
		ksession2.insert(product3);
		
		
		ksession2.startProcess("com.vtrio.process.TestJbpm");

		ksession2.fireAllRules();

	}

}
