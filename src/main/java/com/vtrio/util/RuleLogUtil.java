package com.vtrio.util;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vtrio.model.Product;

public class RuleLogUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RuleLogUtil.class);

	public static void logRule(String ruleName, Product product) {
		StringBuilder logMessage = new StringBuilder();
		logMessage.append("\t Name of the Rule Intercepted is : ");
		logMessage.append(ruleName);
		logMessage.append("\t");
		logMessage.append("Product information : \t ");
		logMessage.append(" Product Id: ");
		logMessage.append(product.getProductId());
		logMessage.append(", Product name : ");
		logMessage.append(product.getProductName());
		logMessage.append(", Product Type: ");
		logMessage.append(product.getProductType());
		logMessage.append(": Discount is updated to ");
		logMessage.append(product.getDiscount());
		logMessage.append(" % .");

		LOGGER.info(new Date() + logMessage.toString());
	}

}
