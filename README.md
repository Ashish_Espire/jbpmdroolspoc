# Getting Started

### Reference Documentation
It is a maven project on Jbpm Drools POC

1) There are two class for testing of the rules

   A ) com.vtrio.ruleinvoker.ProductRuleMain.java (Creates some java dto and fires rule on it)
	         
				
   B) com.vtrio.ruleinvoker.InventoryOfferMain.java (reades data resources/data/products.csv , parse it in java objects and fires
		rules on it.)
	
			

2) Two rules files has been used a) rule.drl and rules.xls

   A) rule.drl defines discount if product is on sale (Sles offer) (or) order date is 25 dec (XMAS offer)
	   
   B) rules.xls define discount on productType (gold,platinum, silver and diamond)	and price range   
   
      Example : if productType is diamond and price is >=15000 and price < 100000l discout is 15
	            if productType is gold and price is >=10000 and price < 50000l discout is 14
				
	Note : 
	1) Rule File path is src/main/resources/com.vtrio.process
	2) Jbpm process which is invoking this rule is defined in src/main/resources/com.vtrio.process.TestJbpm.bpmn2
	3) Data source is defined in resources/data/product.csv
		   
		   


 	

